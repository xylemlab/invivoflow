%This script  drives the flow simulations on the CT-derived network and
%their optimisation to match NMR observations

%choose options here:

x0=-30;   %starting value for pressure difference across stem [Pa]
bounds=[-1e4 0]; %bounds on pressure difference across stem [Pa]
mask='n'; %set to either 'y' or 'n' (default) 
          %switch controls masking out of outer domain from calculations
          %and objective function.
res='lc'; %set to either 'tf' or 'lc' to determine how residuals are treated
          %tf aggregates them all to optimise total flow
          %lc (default) treats residuals locally, averaged over a 5x5 pixel
          %window.
simDir=strcat(pwd,filesep); %by default, the simulation directory is the current directory
                            %and needs to contain the entire repository
                            %contents.

%to run, requires flowPObj.f90 to be compiled/linked in the current directory
[modPix,obsPix,M,O]=runModuFlow(simDir,mask,res,x0,bounds);
%modPix contains the raw output of the simulation
%obsPix flow rates observed by NMR
%M and O contain these same data, smoothed (averaged) over 5x5 pixel windows.

%To enable vessel-scale analysis, need to re-read the flowPObj outputs,
%using:
file='flowExp00001.am'; %deafult name of export file
[vertCrds,nVert,edges,nEdge,pts,nPts,ptCrds,radii,press,flow]=flowExportRead(simDir,file);

%and separate them out by vessel with:
[topP,modFlow,vessRad,vessXYZ,vessDVL,vessIO]=sepByVessel(vertCrds,edges,flow,press);
%outputs:
    %topP: pressure in a vessel at the top of the domain
    %modFlow: %flow in a vessel at the top of the domain
    %vessRad: %mean vessel radius
    %vessXYZ: %vessel coordinate at the top of the domain
    %vessDVL: %true for vessels in the dorsal/ventral domain
    %vessIO:  %true for vessels in the inner domain
    
