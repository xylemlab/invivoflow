function [modPix,obsPix,M,O]=runModuFlow(simDir,mask,res,x0,bounds)
%runModuFlow(base,projDir,pset,res,seg,x0)
%This function implements a parallel optimisation algorithm for flow
%using control parameters for modular choice of objectives

%% Load network domain and NMR observations, set parameters and options

    inp=load('inputs');

    LB=bounds(1)/inp.c;
    UB=bounds(2)/inp.c;
    X0=x0/inp.c;

    opts=optimoptions('lsqnonlin','Display','iter','UseParallel',false,'MaxIter',50,'MaxFunEval',10000);
    
%% Run optimisaton    

    [X,~]=lsqnonlin(@(X)flowModulObj(X,mask,res,simDir,inp),X0,LB,UB,opts);

%% generate best-fit results for output
    [modPix,M,O]=modRes(X,mask,simDir,inp);
    obsPix=inp.obsPix;
end