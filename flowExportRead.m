function [vertCrds,nVert,edges,nEdge,pts,nPts,ptCrds,radii,press,flow]=flowExportRead(dir,file)


    fid=fopen(strcat(dir,file),'r');

    %burn 3 (header)
    for i=1:3
        line=fgetl(fid);
    end

    line=fgetl(fid);
    c=regexp(line,'\d+','match');
    nVert=str2double(c{1});

    line=fgetl(fid);
    c=regexp(line,'\d+','match');
    nEdge=str2double(c{1});

    line=fgetl(fid);
    c=regexp(line,'\d+','match');
    nPts=str2double(c{1});

    for i=1:14
        line=fgetl(fid);
    end

    line=fgetl(fid);

    if ~strcmp(line,'@1')
        throw(MException('exportRead:not@1','Vertex Coordinates not found'))
    end

    vertCrds=zeros(nVert,3);
    i=1;
    line=fgetl(fid);
    while ~isempty(line)
        c=regexp(line,'[\d\.E-+]+','match');
        vertCrds(i,:)=str2double(c);
        i=i+1;
        line=fgetl(fid);
    end
    if i~=nVert+1
        throw(MException('exportRead:nVert','Wrong number of vertices'))
    end


    line=fgetl(fid);

    if ~strcmp(line,'@2')
        throw(MException('exportRead:not@1','Edge ends not found'))
    end

    edges=zeros(nEdge,2);
    i=1;
    line=fgetl(fid);
    while ~isempty(line)
        c=regexp(line,'[\d\.]+','match');
        edges(i,:)=str2double(c);
        i=i+1;
        line=fgetl(fid);
    end
    if i~=nEdge+1
        throw(MException('exportRead:nEdge','Wrong number of edges'))
    end

    line=fgetl(fid);
    if ~strcmp(line,'@3')
        throw(MException('exportRead:not@3','Point data not found'))
    end
    pts=zeros(nEdge,1);
    i=1;
    line=fgetl(fid);
    while ~isempty(line)
        c=regexp(line,'[\d\.]+','match');
        pts(i)=str2double(c);
        i=i+1;
        line=fgetl(fid);
    end
    if i~=nEdge+1
        throw(MException('exportRead:nPts','Wrong number of points'))
    end

    line=fgetl(fid);
    if ~strcmp(line,'@4')
        throw(MException('exportRead:not@4','Point coordinates not found'))
    end
    ptCrds=zeros(nPts,3);
    i=1;
    line=fgetl(fid);
    while ~isempty(line)
        c=regexp(line,'[\d\.E-+]+','match');
        ptCrds(i,:)=str2double(c);
        i=i+1;
        line=fgetl(fid);
    end
    if i~=nPts+1
        throw(MException('exportRead:nPts','Wrong number of points'))
    end


    line=fgetl(fid);
    if ~strcmp(line,'@5')
        throw(MException('exportRead:not@5','Thickness not found'))
    end
    radii=zeros(nPts,1);
    i=1;
    line=fgetl(fid);
    while ~isempty(line)
        c=regexp(line,'[\d\.E-+]+','match');
        radii(i)=str2double(c);
        i=i+1;
        line=fgetl(fid);
    end
    if i~=nPts+1
        throw(MException('exportRead:nPts','Wrong number of points'))
    end

    line=fgetl(fid);
    if ~strcmp(line,'@6')
        throw(MException('exportRead:not@6','Pressure not found'))
    end
    press=zeros(nVert,1);
    i=1;
    line=fgetl(fid);
    while ~isempty(line)
        c=regexp(line,'[\d\.E-+]+','match');
        if numel(c)==1
            press(i)=str2double(c);
        end
        i=i+1;
        line=fgetl(fid);
    end
    if i~=nVert+1
        throw(MException('exportRead:nVerts','Wrong number of vertices'))
    end

    line=fgetl(fid);
    if ~strcmp(line,'@7')
        throw(MException('exportRead:not@7','Flow not found'))
    end
    flow=zeros(nEdge,1);
    i=1;
    line=fgetl(fid);
    while ~(isempty(line) || isnumeric(line))
        if any(regexp(line,'\*'))
            flow(i)=NaN;
        else
            c=regexp(line,'[\d\.E-+]+','match');
            flow(i)=str2double(c);
        end
        i=i+1;
        line=fgetl(fid);
    end
    if i~=nEdge+1
        throw(MException('exportRead:nEdge','Wrong number of edges'))
    end

    fclose(fid);

end
