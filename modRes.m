function [modPix,M,O]=modRes(X,mask,dirName,inp)


    eP=repmat(inp.botPress,[inp.nTop 1]); %set top pressures
    switch mask
        case 'n'
            eP=eP+X*inp.c*inp.endZ;
        case 'y'
            eP(inp.endMask)=eP(inp.endMask)+X*inp.c*inp.endZ(inp.endMask);
    end
    
    modPix=flowDriver(eP,dirName,inp.inds,inp.MR,inp.VS,inp.eInd,...
        inp.nEdg,inp.nVess,inp.szA);
    
    [M,O]=smoothRes(modPix,inp.obsPix); %smooth residuals in 5x5 window
    
end