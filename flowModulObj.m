function J=flowModulObj(X,mask,res,dirName,inp)


    eP=repmat(inp.botPress,[inp.nTop 1]); %set top pressures
    switch mask
        case 'n'
            eP=eP+X*inp.c*inp.endZ;
        case 'y'
            eP(inp.endMask)=eP(inp.endMask)+X*inp.c*inp.endZ(inp.endMask);
    end
    
    modPix=flowDriver(eP,dirName,inp.inds,inp.MR,inp.VS,inp.eInd,...
        inp.nEdg,inp.nVess,inp.szA);
    
    [M,O]=smoothRes(modPix,inp.obsPix); %smooth residuals in 5x5 window

    switch strcat(res,mask)
        case 'tfn'
            J=sum(inp.o*(M-O)); % total flow objective on entire domain
        case 'tfy'
            J=sum(inp.o*(M(inp.nmrMask)-O(inp.nmrMask))); % total flow objective on masked subdomain
        case 'lcn'
            J=inp.o*(M-O); %local, smoothed objective on entire domain
        case 'lcy'
            J=inp.o*(M(inp.nmrMask)-O(inp.nmrMask)); %local, smoothed objective on masked subdomain
    end
    
end

    