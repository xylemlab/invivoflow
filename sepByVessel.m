function [topP,modFlow,vessRad,vessXYZ,vessDVL,vessIO]=sepByVessel(vertCrds,edges,flow,press)
    
    sp=load('spatial');
    inp=load('inputs','eInd','MR');
    
    [lia,locb]=ismember(vertCrds,sp.topCrd,'rows');
    topVert=find(lia)-1;

    [lia,locb]=ismember(edges(:,2),topVert);
    myVerts=edges(lia,:)+1;
    

    
    myEdge=find(lia);
    [~,myVess]=ismember(myEdge,cat(1,inp.eInd{:}));
    modFlow=zeros(491,1);
    topP=zeros(491,1);
    vessXYZ=zeros(491,3);
    for i=1:491
        crd=vertCrds(myVerts(i,:),:);
        vessXYZ(i,:)=crd(2,:);
        vessXYZ(i,1:2)=vessXYZ(i,1:2)-660*3.2e-6;
        modFlow(i)=flow(myEdge(i));
        topP(i)=press(myVerts(i,2));
    end
        
    vessRad=inp.MR(myVess);
    vessDVL=sp.vessDVL;
    vessIO=sp.vessIO;
    
   

end
