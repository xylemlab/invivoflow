function [M,O]=smoothRes(modPix,obsPix)

    O=zeros(61^2,1);
    M=zeros(61^2,1);
	for i=3:63
        for j=3:63
            sten=[(i-2:i+2)' (j-2:j+2)'];
            M((j-3)*61+(i-2))=mean(mean(modPix(sten(:,1),sten(:,2))));
            O((j-3)*61+(i-2))=mean(mean(obsPix(sten(:,1),sten(:,2))));
        end
	end
    
end