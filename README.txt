This repository contains the source code and data necessary to replicate the work in the manucsript "In-vivo flow redirection from wide to narrow xylem vessels due to pressure gradient heterogeneity."

License
See the LICENSE.txt file for rights and limitations (CC BY-NC-SA 4.0).


Software Dependencies
The software was compiled and ran within the CentOS (v7.1) operating system. 
It requires a licensed, installed copy of Matlab (R2017b) as well as the Optimization toolbox (both MathWorks) to run.
A fortran compiler is also required for installation.
Only the version 0.1 (initial commit) has been tested to execute correctly on the authors' machine.
No non-standard hardware is required for the operation of this software.


Installation
To obtain the simplest working copy of the software:
	place or link the entire repository contents into the same directory,
	compile flowPObj.f90 using your chosen compiler and the output file name flowPObj.
Expected installation time is less than ten minutes. 

Running
Open matlab an navigate to the directory with all the matlab source code, compiled flowPObj programme and data. All analyses are run from the script drive_CT_NMR; all other matlab source files are functions executed from this file. Once compiled, the fortran programme is executed from within the matlab modular objective function. Furhter notes on code structure, execution, and expected results are found in the driver file as comments. The demo data are the study data and the expected outputs are the study results.

Data
The actual study outputs (data contained in manuscript figures) are in the folder outputs, with a description there.

