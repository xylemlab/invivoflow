MODULE nrtype
  INTEGER, PARAMETER :: I4B = SELECTED_INT_KIND(9)
  INTEGER, PARAMETER :: I2B = SELECTED_INT_KIND(4)
  INTEGER, PARAMETER :: I1B = SELECTED_INT_KIND(2)
  INTEGER, PARAMETER :: SP = KIND(1.0)
  INTEGER, PARAMETER :: DP = KIND(1.0D0)
  INTEGER, PARAMETER :: SPC = KIND((1.0,1.0))
  INTEGER, PARAMETER :: DPC = KIND((1.0D0,1.0D0))
  INTEGER, PARAMETER :: LGT = KIND(.true.)
  REAL, PARAMETER :: PI=3.141592653589793238462643383279502884197_sp
  
  TYPE sprs2_dp
     INTEGER :: n,len
     DOUBLE PRECISION, DIMENSION(:), POINTER :: val
     INTEGER, DIMENSION(:), POINTER :: irow
     INTEGER, DIMENSION(:), POINTER :: jcol
  END TYPE sprs2_dp
  
END MODULE nrtype


MODULE nr
  
  INTERFACE
     SUBROUTINE asolve(b,x,itrnsp)
       USE nrtype
       DOUBLE PRECISION, DIMENSION(:), INTENT(IN) :: b
       DOUBLE PRECISION, DIMENSION(:), INTENT(OUT) :: x
       INTEGER, INTENT(IN) :: itrnsp
     END SUBROUTINE asolve
  END INTERFACE
  INTERFACE
     SUBROUTINE atimes(x,r,itrnsp)
       USE nrtype
       DOUBLE PRECISION, DIMENSION(:), INTENT(IN) :: x
       DOUBLE PRECISION, DIMENSION(:), INTENT(OUT) :: r
       INTEGER, INTENT(IN) :: itrnsp
     END SUBROUTINE atimes
  END INTERFACE
  INTERFACE
     FUNCTION snrm(sx,itol)
       USE nrtype
       DOUBLE PRECISION, DIMENSION(:), INTENT(IN) :: sx
       INTEGER, INTENT(IN) :: itol
       DOUBLE PRECISION :: snrm
     END FUNCTION snrm
  END INTERFACE
  INTERFACE
     SUBROUTINE sprsax(sa,x,b)
       USE nrtype
       TYPE(sprs2_dp), INTENT(IN) :: sa
       DOUBLE PRECISION, DIMENSION (:), INTENT(IN) :: x
       DOUBLE PRECISION, DIMENSION (:), INTENT(OUT) :: b
     END SUBROUTINE sprsax
  END INTERFACE
  INTERFACE
     SUBROUTINE sprsdiag(sa,b)
       USE nrtype
       TYPE(sprs2_dp), INTENT(IN) :: sa
       DOUBLE PRECISION, DIMENSION(:), INTENT(OUT) :: b
     END SUBROUTINE sprsdiag
  END INTERFACE
  INTERFACE
     SUBROUTINE sprstx(sa,x,b)
       USE nrtype
       TYPE(sprs2_dp), INTENT(IN) :: sa
       DOUBLE PRECISION, DIMENSION (:), INTENT(IN) :: x
       DOUBLE PRECISION, DIMENSION (:), INTENT(OUT) :: b
     END SUBROUTINE sprstx
  END INTERFACE
  
END MODULE nr



MODULE xlinbcg_data
  USE nrtype
  TYPE(sprs2_dp) :: sa
END MODULE xlinbcg_data


MODULE xlinbcg_data_pre
  USE nrtype
  TYPE(sprs2_dp) :: sapre
END MODULE xlinbcg_data_pre

MODULE nrutil
  USE nrtype
  IMPLICIT NONE
  INTEGER, PARAMETER :: NPAR_GEOP=4,NPAR2_GEOP=2
  INTEGER, PARAMETER :: NPAR_CUMSUM=16
  INTEGER, PARAMETER :: NPAR_CUMPROD=8
  INTEGER, PARAMETER :: NPAR_POLY=8
  INTEGER, PARAMETER :: NPAR_POLYTERM=8
  INTERFACE array_copy
     MODULE PROCEDURE array_copy_r, array_copy_d, array_copy_i
  END INTERFACE
  
  INTERFACE assert
     MODULE PROCEDURE assert1,assert2,assert3,assert4,assert_v
  END INTERFACE
  INTERFACE assert_eq
     MODULE PROCEDURE assert_eq2,assert_eq3,assert_eq4,assert_eqn
  END INTERFACE
  
  INTERFACE scatter_add
     MODULE PROCEDURE scatter_add_r,scatter_add_d
  END INTERFACE
  
CONTAINS
  
  SUBROUTINE array_copy_r(src,dest,n_copied,n_not_copied)
    REAL, DIMENSION(:), INTENT(IN) :: src
    REAL, DIMENSION(:), INTENT(OUT) :: dest
    INTEGER, INTENT(OUT) :: n_copied, n_not_copied
    n_copied=min(size(src),size(dest))
    n_not_copied=size(src)-n_copied
    dest(1:n_copied)=src(1:n_copied)
  END SUBROUTINE array_copy_r
  
  SUBROUTINE array_copy_d(src,dest,n_copied,n_not_copied)
    DOUBLE PRECISION, DIMENSION(:), INTENT(IN) :: src
    DOUBLE PRECISION, DIMENSION(:), INTENT(OUT) :: dest
    INTEGER, INTENT(OUT) :: n_copied, n_not_copied
    n_copied=min(size(src),size(dest))
    n_not_copied=size(src)-n_copied
    dest(1:n_copied)=src(1:n_copied)
  END SUBROUTINE array_copy_d
  
  SUBROUTINE array_copy_i(src,dest,n_copied,n_not_copied)
    
    INTEGER, DIMENSION(:), INTENT(IN) :: src
    INTEGER, DIMENSION(:), INTENT(OUT) :: dest
    INTEGER, INTENT(OUT) :: n_copied, n_not_copied
    n_copied=min(size(src),size(dest))
    n_not_copied=size(src)-n_copied
    dest(1:n_copied)=src(1:n_copied)
  END SUBROUTINE array_copy_i
  
  SUBROUTINE assert1(n1,string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    LOGICAL, INTENT(IN) :: n1
    if (.not. n1) then
       write (*,*) 'nrerror: an assertion failed with this tag:', string
       STOP 'program terminated by assert1'
    end if
  END SUBROUTINE assert1
  
  SUBROUTINE assert2(n1,n2,string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    LOGICAL, INTENT(IN) :: n1,n2
    if (.not. (n1 .and. n2)) then
       write (*,*) 'nrerror: an assertion failed with this tag:', string
       STOP 'program terminated by assert2'
    end if
  END SUBROUTINE assert2
  
  SUBROUTINE assert3(n1,n2,n3,string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    LOGICAL, INTENT(IN) :: n1,n2,n3
    if (.not. (n1 .and. n2 .and. n3)) then
       write (*,*) 'nrerror: an assertion failed with this tag:',string
       STOP 'program terminated by assert3'
    end if
  END SUBROUTINE assert3
  
  SUBROUTINE assert4(n1,n2,n3,n4,string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    LOGICAL, INTENT(IN) :: n1,n2,n3,n4
    if (.not. (n1 .and. n2 .and. n3 .and. n4)) then
       write (*,*) 'nrerror: an assertion failed with this tag:', string
       STOP 'program terminated by assert4'
    end if
  END SUBROUTINE assert4
  
  SUBROUTINE assert_v(n,string)
    
    CHARACTER(LEN=*), INTENT(IN) :: string
    LOGICAL, DIMENSION(:), INTENT(IN) :: n
    if (.not. all(n)) then
       write (*,*) 'nrerror: an assertion failed with this tag:', string
       STOP 'program terminated by assert_v'
    end if
  END SUBROUTINE assert_v
  
  FUNCTION assert_eq2(n1,n2,string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    INTEGER, INTENT(IN) :: n1,n2
    INTEGER :: assert_eq2
    if (n1 == n2) then
       assert_eq2=n1
    else
       write (*,*) 'nrerror: an assert_eq failed with this tag:', string
       STOP 'program terminated by assert_eq2'
    end if
  END FUNCTION assert_eq2
  FUNCTION assert_eq3(n1,n2,n3,string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    INTEGER, INTENT(IN) :: n1,n2,n3
    INTEGER :: assert_eq3
    if (n1 == n2 .and. n2 == n3) then
       assert_eq3=n1
    else
       write (*,*) 'nrerror: an assert_eq failed with this tag:', string
       STOP 'program terminated by assert_eq3'
    end if
  END FUNCTION assert_eq3
  
  FUNCTION assert_eq4(n1,n2,n3,n4,string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    INTEGER, INTENT(IN) :: n1,n2,n3,n4
    INTEGER :: assert_eq4
    if (n1 == n2 .and. n2 == n3 .and. n3 == n4) then
       assert_eq4=n1
    else
       write (*,*) 'nrerror: an assert_eq failed with this tag:', string
       STOP 'program terminated by assert_eq4'
    end if
  END FUNCTION assert_eq4
  
  FUNCTION assert_eqn(nn,string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    INTEGER, DIMENSION(:), INTENT(IN) :: nn
    INTEGER :: assert_eqn
    if (all(nn(2:) == nn(1))) then
       assert_eqn=nn(1)
    else
       write (*,*) 'nrerror: an assert_eq failed with this tag:', string
       STOP 'program terminated by assert_eqn'
    end if     
  END FUNCTION assert_eqn

  SUBROUTINE nrerror(string)
    CHARACTER(LEN=*), INTENT(IN) :: string
    write (*,*) 'nrerror: ',string
    STOP 'program terminated by nrerror'
  END SUBROUTINE nrerror
  
  SUBROUTINE scatter_add_r(dest,source,dest_index)
    REAL, DIMENSION(:), INTENT(OUT) :: dest
    REAL, DIMENSION(:), INTENT(IN) :: source
    INTEGER, DIMENSION(:), INTENT(IN) :: dest_index
    INTEGER :: m,n,j,i
    n=assert_eq2(size(source),size(dest_index),'scatter_add_r')
    m=size(dest)
    do j=1,n
       i=dest_index(j)
       if (i > 0 .and. i <= m) dest(i)=dest(i)+source(j)
    end do
  END SUBROUTINE scatter_add_r
  
  SUBROUTINE scatter_add_d(dest,source,dest_index)
    DOUBLE PRECISION, DIMENSION(:), INTENT(OUT) :: dest
    DOUBLE PRECISION, DIMENSION(:), INTENT(IN) :: source
    INTEGER, DIMENSION(:), INTENT(IN) :: dest_index
    INTEGER :: m,n,j,i
    n=assert_eq2(size(source),size(dest_index),'scatter_add_d')
    m=size(dest)
    do j=1,n
       i=dest_index(j)
       if (i > 0 .and. i <= m) dest(i)=dest(i)+source(j)
    end do
  END SUBROUTINE scatter_add_d
   
END MODULE nrutil

MODULE gcmmn
  DOUBLE PRECISION, ALLOCATABLE :: x(:),y(:),z(:)
  INTEGER, ALLOCATABLE :: edge_connectivity1(:)
  INTEGER, ALLOCATABLE :: edge_connectivity2(:)
  INTEGER, ALLOCATABLE :: num_edge_points(:)
  INTEGER :: max_num_points_per_seg
  DOUBLE PRECISION :: shared_pit_length
  INTEGER :: number_top_nodes,number_bott_nodes
  INTEGER :: num_nodes,num_segments,num_iterations
  INTEGER ::  number_int_nodes,num_initial_emb,idum_save
  DOUBLE PRECISION :: tol, convert_factor
  DOUBLE PRECISION :: end_conductivity
  DOUBLE PRECISION :: pit_conductivity,bottom_pressure
  DOUBLE PRECISION :: pit_fraction
  DOUBLE PRECISION :: voxel_size,std_pore,avg_max_pore
  DOUBLE PRECISION :: vess_end_prob
  DOUBLE PRECISION :: flowtot_bef,pit_width,pit_height
  LOGICAL, ALLOCATABLE :: pit_tag(:)
  LOGICAL, ALLOCATABLE :: end_tag(:)
  LOGICAL, ALLOCATABLE :: embolize_tag(:)
  LOGICAL, ALLOCATABLE :: bridge_tag(:)
  DOUBLE PRECISION,ALLOCATABLE :: coordx(:,:),coordy(:,:)
  DOUBLE PRECISION, ALLOCATABLE :: radius(:,:),coordz(:,:)
  DOUBLE PRECISION, ALLOCATABLE :: pitlength(:)
  DOUBLE PRECISION, ALLOCATABLE :: sparse_numbers(:,:)
  INTEGER, ALLOCATABLE :: sparse_index(:)
  INTEGER, ALLOCATABLE :: number_ends(:)
  INTEGER, ALLOCATABLE :: sparse_pos(:,:)
  INTEGER :: sampling_freq,lengthing,itmax
  LOGICAL, ALLOCATABLE :: emb_node(:)
  LOGICAL, ALLOCATABLE :: noflowtag(:)
  DOUBLE PRECISION, ALLOCATABLE :: xsol_noemb(:)
  DOUBLE PRECISION, ALLOCATABLE :: flowrate(:)
  INTEGER, ALLOCATABLE :: bottsegs(:),topsegs(:)
  DOUBLE PRECISION, ALLOCATABLE :: large_pore_seg(:)
  DOUBLE PRECISION, ALLOCATABLE :: PLCstore(:)
  DOUBLE PRECISION, ALLOCATABLE :: Negstore(:)
  INTEGER :: plccounter,iter_count,vess_iter
  INTEGER :: num_vess_iter,init_emb_size_pref,init_emb_radius_pref
  INTEGER :: press_count,num_press_steps
  INTEGER :: pressure_continuation,vess_end_count
  DOUBLE PRECISION :: incr_press_step,bridge_conductivity,radial_band
  DOUBLE PRECISION :: incr_vessend_step,vess_element_length
  DOUBLE PRECISION :: xcenter,ycenter,vess_end_prob_original
  INTEGER :: bridges,num_vessend_steps,vessend_continuation,num_bridge_segs
  INTEGER, ALLOCATABLE :: specify_node(:)
  DOUBLE PRECISION, ALLOCATABLE :: top_bdry_press(:)

END MODULE gcmmn

PROGRAM convert_am_file
  USE gcmmn
  IMPLICIT NONE
  INTEGER :: i,j    
  
  !     Read input files
  call read_input
  
  !     Initialize any variables that don't change based on vessel ends
  call initialize
       
  !     Calculate conductivity and connectivity
  call segments

  !     Establish the largest pores in the pit membranes: vestigial, can remove?
  call embolize_init
           
  !     Flow calculations without embolism
  call calc_flow_bef
           
  !     Export results
  call avizo_export 

  !     Deallocate variables
  call outro
           
END PROGRAM convert_am_file
         
         
         


      SUBROUTINE read_input
      USE gcmmn
      IMPLICIT NONE
      INTEGER :: i,j,k,p,edgetemp,counter
      INTEGER :: q,n,edgeptcount,iostatus,bridge_pit_tag
      DOUBLE PRECISION :: dist
      DOUBLE PRECISION :: point_thick
      INTEGER, ALLOCATABLE :: bridge_segs(:)
      character (len=20):: vertex
      character (len=18):: edge
      character (len=6) :: vertex2,edge2
      real :: a,b,c

      
      open(unit=10,file='Inputs_flow.txt',status='old')

!     The conductivity of the vessel endings and then the intervessel pits (in units of FILL IN THE BLANK)
      read(10,*) end_conductivity
      read(10,*) pit_conductivity
!     Fraction of the pit area that is pit membrane
      read(10,*) pit_fraction
      IF((pit_fraction.gt.1).or.(pit_fraction.lt.0)) then
         PRINT *, 'ERROR: Invalid input. Pit fraction must be between 0 and 1.'
            pause
      end if
!     The average height of a pit membrane
      read(10,*) pit_height
      If(pit_height.lt.0)then	
	PRINT *, 'ERROR: Invalid input. Average pit height must be greater than 0.'
        pause
      end if
!     The average width of a pit membrane
      read(10,*) pit_width  
      If(pit_width.lt.0)then	
	PRINT *, 'ERROR: Invalid input. Average pit width must be greater than 0.'
        pause
      end if
      !Vessel element length
      read(10,*) vess_element_length
        If(vess_element_length.lt.0)then	
	PRINT *, 'ERROR: Invalid input. Vessel element length must be greater than 0.'
        pause
      end if
!     The average maximum pore size in a given area (voxel size *  100 microns)
      read(10,*) avg_max_pore  
      If(avg_max_pore.lt.0)then	
	PRINT *, 'ERROR: Invalid input. Average maximum pore size must be greater than 0.'
        pause
      end if 
!     The standard deviation of the average maximum pore size
      read(10,*) std_pore
      If(std_pore.lt.0)then	
	PRINT *, 'ERROR: Invalid input. Standard deviation of pore size must be greater than 0.'
        pause
      end if
!     The boundary conditions--pressures at the bottom and top of the domain
      read(10,*) bottom_pressure

!     Tolerance for solution
      read(10,*) tol
      If(tol.lt.0)then	
	PRINT *, 'ERROR: Invalid input. Tolerance must be greater than 0.'
      end if
!     Maximum number of iterations to find a converged solution
      read(10,*,IOSTAT=IOSTATUS) itmax
      If(itmax.lt.0)then	
	PRINT *, 'ERROR: Invalid input. Maximum iterations must be greater than 0.'
      end if
      IF(IOSTATUS>0) then
         PRINT *, 'ERROR: Invalid input. Maximum number of iterations must be an integer.'
         pause
      end if
      

!     open file that was saved by network.x in order to read parameters for center coordinates, voxel size, and sampling frequency for intervessel pit connections      
      open(unit=74,file='flowparam.txt',status='old')
      read(74,*) convert_factor
      read(74,*) xcenter
      read(74,*) ycenter
      read(74,*) voxel_size
      read(74,*) sampling_freq
      read(74,*) bridges
      close(74)

      if(bridges.eq.1) then
         read(10,*) bridge_conductivity
         read(10,*) bridge_pit_tag
!     Area specific conductivities
         bridge_conductivity=bridge_conductivity*pit_fraction
      end if
      close(10)
!     Area specific conductivities
      pit_conductivity=pit_conductivity*pit_fraction
     

      open(unit=11,file='avizo_flow_input.am',status='old')
      
!     Read intro part of file
      
      do i=1,3
         read(11,*)
      end do
      read(11,"(A20)") vertex
      read(11,"(A18)") edge
      
      
      do i=1,15
         read(11,*)
      end do
      
      vertex2=vertex(15:20)
      edge2=edge(13:18)
      
      read(vertex2,*) num_nodes
      read(edge2,*) num_segments
      
      ALLOCATE(x(num_nodes))
      ALLOCATE(y(num_nodes))
      ALLOCATE(z(num_nodes))
      ALLOCATE(edge_connectivity1(num_segments))
      ALLOCATE(edge_connectivity2(num_segments))
      ALLOCATE(num_edge_points(num_segments))
      
!     Coordinates of vertices
      do 12 i=1,num_nodes
         read(11,*,iostat=k) x(i),y(i),z(i)
!     convert units to meters
         x(i)=x(i)*convert_factor
         y(i)=y(i)*convert_factor
         z(i)=z(i)*convert_factor
 12   continue
      read(11,*)
      read(11,*)
      

      
!     Connections between vertices
      do 13 i=1,num_segments
         read(11,*,iostat=k) edge_connectivity1(i),edge_connectivity2(i)
         IF(k>0) exit
!     Add one because the export format starts with index 0
         edge_connectivity1(i)=edge_connectivity1(i)+1
         edge_connectivity2(i)=edge_connectivity2(i)+1
         
 13   continue
      
!     Designate nodes for other subroutines
      call specify_nodes

      read(11,*)
      read(11,*)
      
      num_edge_points=0

!     Number of edge points between the nodes of each segment
      do 14 i=1,num_segments
         read(11,*) num_edge_points(i)
 14   continue
      
      read(11,*)
      read(11,*)

      max_num_points_per_seg=maxval(num_edge_points)
      ALLOCATE(coordx(num_segments,max_num_points_per_seg))
      ALLOCATE(coordy(num_segments,max_num_points_per_seg))
      ALLOCATE(coordz(num_segments,max_num_points_per_seg))
      ALLOCATE(radius(num_segments,max_num_points_per_seg))
      ALLOCATE(pitlength(num_segments))
      
!     Initialize variables
      coordx=0
      coordy=0
      coordz=0
      radius=0
      
      
!     Edge point coordinates
      do 15 i=1,num_segments
         do 51 j=1,num_edge_points(i)
            read(11,*) coordx(i,j),coordy(i,j),coordz(i,j)
!     convert to meters
            coordx(i,j)=coordx(i,j)*convert_factor
            coordy(i,j)=coordy(i,j)*convert_factor
            coordz(i,j)=coordz(i,j)*convert_factor
 51      continue
 15   continue
      read(11,*)
      read(11,*)
      
!     Thickness (i.e. radius) of vessel at each edge point	
      do 71 i=1,num_segments
         do 61 j=1,num_edge_points(i)
            read(11,*) point_thick
!     convert units to meters
            radius(i,j)=point_thick*convert_factor
 61      continue
 71   continue
      read(11,*)
      read(11,*) 
    
!     Length of connection
      do i=1,num_segments
         read(11,*) pitlength(i)
      end do
      close(11)

      if(bridges.eq.1) then
         ALLOCATE(bridge_segs(num_segments))
         bridge_segs=0
         open(unit=92,file='bridge_segs.txt',status='old')
         do i=1,num_nodes
            read(92,*,IOSTAT=IOSTATUS) bridge_segs(i)
            if(IOSTATUS<0) then
               num_bridge_segs=i-1
               exit
            end if
         end do
         close(92)
      end if
      ALLOCATE(bridge_tag(num_segments))
      bridge_tag=.false.
!     Find segments that represent bridge pitting
      if((bridges.eq.1).and.(bridge_pit_tag.eq.0)) then
         do i=1,num_segments
            if(bridge_segs(i).ne.0) then
               bridge_tag(bridge_segs(i))=.true.
            end if
         end do
         DEALLOCATE(bridge_segs)
      end if
      counter=0
      do i=1,num_segments
         if(bridge_tag(i).eqv..true.) then
            counter=counter+1
         end if
      end do

      END SUBROUTINE read_input
      
      
      SUBROUTINE specify_nodes
      USE gcmmn
      IMPLICIT NONE
      INTEGER :: i,k,node_counter,topnode,bottomnode
      LOGICAL :: edge1,edge2
      

      
!     Specify the nodes as internal, top, or bottom nodes (useful for later subroutines)

      number_int_nodes=0
      number_top_nodes=0
      number_bott_nodes=0
      

      do 32 i=1,num_nodes
         node_counter=0
         edge1=.false.
         edge2=.false.
         do k=1,num_segments
            if(edge_connectivity1(k).eq.i) then
               node_counter=node_counter+1
               edge1=.true.
            elseif(edge_connectivity2(k).eq.i) then
               node_counter=node_counter+1
               edge2=.true.
            end if
            if(k.eq.num_segments) then
               if(node_counter.eq.1) then
                  if(edge1.eqv..true.) then
                     number_bott_nodes=number_bott_nodes+1
                  elseif(edge2.eqv..true.) then
                     number_top_nodes=number_top_nodes+1
                  end if
               else
                  number_int_nodes=number_int_nodes+1
               end if
            end if
         end do
 32   continue

      
      open(unit=29,file='top_nodes.txt',status='old')
      open(unit=30,file='bottom_nodes.txt',status='old')
      open(unit=61,file='top_press.txt',status='old')
      
      ALLOCATE(specify_node(num_nodes))
      ALLOCATE(top_bdry_press(num_nodes))
      specify_node=0
!     Bottom nodes are 1, top nodes are 2, internal nodes are zero
      do i=1,number_bott_nodes
         read(30,*) bottomnode
         specify_node(bottomnode)=1
      end do
      do i=1,number_top_nodes
         read(29,*) topnode
         specify_node(topnode)=2
         read(61,*) top_bdry_press(topnode)
      end do

      close(29)
      close(30)
      close(61)
      
      END SUBROUTINE specify_nodes
      
      SUBROUTINE initialize
      USE gcmmn
      IMPLICIT NONE
      INTEGER :: j,idum,k,n

      vess_iter=1

      !     Allocate necessary arrays
      ALLOCATE(pit_tag(num_segments))
      ALLOCATE(large_pore_seg(num_segments))
      ALLOCATE(bottsegs(number_bott_nodes))
      ALLOCATE(topsegs(number_top_nodes))
      ALLOCATE(noflowtag(num_nodes))
      ALLOCATE(xsol_noemb(num_nodes))
      ALLOCATE(flowrate(num_segments))
      ALLOCATE(end_tag(num_segments))
      ALLOCATE(sparse_pos(num_nodes,12))
      ALLOCATE(sparse_numbers(num_nodes,12))
      ALLOCATE(number_ends(num_segments))
      ALLOCATE(embolize_tag(num_segments))
      ALLOCATE(emb_node(num_nodes))

      if(num_iterations.gt.0) then
         ALLOCATE(PLCstore(num_iterations*num_vess_iter))
         ALLOCATE(negstore(num_iterations*num_vess_iter))
      else
         ALLOCATE(negstore(num_vess_iter))
      end if
      ALLOCATE(sparse_index(num_nodes))
      pit_tag=.false.
      do 49 j=1,num_segments
         if((abs((z(edge_connectivity1(j))-z(edge_connectivity2(j)))).lt.(0.5*voxel_size)).and.(bridge_tag(j).eqv..false.))then
            pit_tag(j)=.true.
         end if
 49   continue
      
!     This gives the number of seconds since 1970 (used for random no. generation)
      idum=time()
      idum_save=idum
      
      open(unit=19,file='bottom_nodes.txt',status='old')
      open(unit=89,file='top_nodes.txt',status='old')

      do 21 k=1,number_bott_nodes
         read(19,*) j
         
         do 22 n=1,num_segments
            if(edge_connectivity1(n).eq.j) THEN
               bottsegs(k)=n
               GOTO 26
            end if
 22      continue
 26      continue
 21   continue

      if(num_initial_emb.gt.number_bott_nodes) then
         PRINT *,'Too many embolisms specified.'
         num_initial_emb=number_bott_nodes
      end if 
      do 85 k=1,number_top_nodes
         read(89,*) j
         
         do 82 n=1,num_segments
            if(edge_connectivity2(n).eq.j) THEN
               topsegs(k)=n
               GOTO 86
            end if
 82      continue
 86      continue
 85   continue
 
!     open necessary files

      open(unit=76,file='plc_prop.txt',status='unknown')     
      open(unit=81,file='plc_var.txt',status='unknown')
      open(unit=83,file='plc_press_var.txt',status='unknown')
      open(unit=31,file='reverse_flow.txt',status='unknown') 
      open(unit=84,file='flowrates.txt',status='unknown')
      open(unit=79,file='flowrates_by_element.txt',status='unknown')
      write(31,*) '% of Vessels with neg. flow     Longest segment with reverse flow'
      close(19)
      close(89)
      
      END SUBROUTINE initialize
      
      SUBROUTINE segments
      USE gcmmn
      USE nrtype
      IMPLICIT NONE
      INTEGER :: i,j,k,m,int_node
      LOGICAL :: marked,non_diag_elem
      INTEGER :: n
!     The assumption is that there will be no more than 12 v-v connections from any one point
      DOUBLE PRECISION, DIMENSION (12*num_nodes) :: sa_begin
      TYPE(sprs2_dp) :: sa
      
      open(unit=47,file='internal_nodes.txt',status='old')
      open(unit=48,file='node_connectivity.txt',status='unknown')
      open(unit=49,file='node_conductivity.txt',status='unknown')
      

!     Initialize variable
      lengthing=0
      
      do i=1,num_nodes
         sparse_numbers(i,:)=0.d0
         sparse_pos(i,:)=0
         sparse_index(i)=0
      end do
      shared_pit_length=sampling_freq*voxel_size
      m=1
      

      if(number_int_nodes.gt.0) then
      do 46 i=1,number_int_nodes
         k=0
         read(47,*) int_node
         
!     Calculate number of segments connected to the node
         do 50 j=1,num_segments
            if(edge_connectivity1(j).eq.int_node) then
               k=k+1
            elseif(edge_connectivity2(j).eq.int_node) then
               k=k+1
            end if
 50      continue
         call conn(int_node,k)
 46   continue

      end if

!     Create sparse matrices for flow calculations
      
      
      do i=1,num_nodes+1
         sa_begin(m)=1
         m=m+1
      end do
      
      n=1
      
      do i=1,num_nodes
         
         marked=.false.
         do j=1,12
            if(sparse_numbers(i,j).ne.0) then
               sa_begin(m)=-sparse_numbers(i,j)
               if(marked.eqv..false.) then
                  sa_begin(i)=sparse_numbers(i,j)
                  marked=.true.
               else
                  sa_begin(i)=sparse_numbers(i,j)+sa_begin(i)
               end if
               m=m+1
            end if
         end do
         
      end do

      lengthing=lengthing+num_nodes
      
      close(47)
      close(48)
      close(49)

      END SUBROUTINE segments
      
      SUBROUTINE conn(node,k)
        USE gcmmn
        IMPLICIT NONE
        DOUBLE PRECISION, ALLOCATABLE:: conductivity(:)
        INTEGER :: i,j,k,m,n,node
        INTEGER, ALLOCATABLE :: node_connection(:)
        DOUBLE PRECISION :: length,pi
        DOUBLE PRECISION :: sum_rad,avg_rad
        
        ALLOCATE (conductivity(k))
        ALLOCATE (node_connection(k))
        

        pi=2.d0*ACOS(0.d0)
        
        conductivity=0
        node_connection=0
        
        m=1
        do 51 j=1,num_segments
           sum_rad=0
           do n=1,num_edge_points(j)
              sum_rad=sum_rad+radius(j,n)
           end do
           avg_rad=sum_rad/num_edge_points(j)
           if(edge_connectivity1(j).eq.node) then
              node_connection(m)=edge_connectivity2(j)
              if(pit_tag(j).eqv..true.)then
                 conductivity(m)=pit_conductivity*shared_pit_length*2*avg_rad*pitlength(j)
              elseif(bridge_tag(j).eqv..true.) then
                 conductivity(m)=bridge_conductivity*shared_pit_length*pitlength(j)*2*avg_rad
              elseif(end_tag(j).eqv..true.) then
                 length=dsqrt((x(node)-x(edge_connectivity2(j)))**2.d0+(y(node)-y(edge_connectivity2(j)))&
                      &**2.d0+(z(node)-z(edge_connectivity2(j)))**2.d0)
                 conductivity(m)=1/(1/(pi*avg_rad**4.d0/(8.d-3*length))+number_ends(j)/(end_conductivity*pi*avg_rad**2.d0))
              else
                 length=dsqrt((x(node)-x(edge_connectivity2(j)))**2.d0+(y(node)-y(edge_connectivity2(j)))&
                      &**2.d0+(z(node)-z(edge_connectivity2(j)))**2.d0)
                 conductivity(m)=pi*avg_rad**4.d0/(8.d-3*length)
              end if
              m=m+1
           elseif(edge_connectivity2(j).eq.node) then
              node_connection(m)=edge_connectivity1(j)
              if(pit_tag(j).eqv..true.)then
                 conductivity(m)=pit_conductivity*shared_pit_length*2*avg_rad*pitlength(j)
             elseif(bridge_tag(j).eqv..true.) then
                 conductivity(m)=bridge_conductivity*shared_pit_length*pitlength(j)*2*avg_rad
              elseif(end_tag(j).eqv..true.) then
                 length=dsqrt((x(node)-x(edge_connectivity1(j)))**2.d0+(y(node)-y(edge_connectivity1(j)))&
                      &**2.d0+(z(node)-z(edge_connectivity1(j)))**2.d0)
                 conductivity(m)=1/(1/(pi*avg_rad**4.d0/(8.d-3*length))+number_ends(j)/(end_conductivity*pi*avg_rad**2.d0))   
              else
                 length=dsqrt((x(node)-x(edge_connectivity1(j)))**2.d0+(y(node)-y(edge_connectivity1(j)))&
                      &**2.d0+(z(node)-z(edge_connectivity1(j)))**2.d0)
                 conductivity(m)=pi*avg_rad**4.d0/(8.d-3*length)
              end if
              m=m+1
           end if
51         continue

           write(48,*) node_connection
           write(49,*) conductivity
           m=m-1

           sparse_index(node)=m
           if(m.gt.12) then
              PRINT *,'ERROR: Vessel has too many connections. Cannot solve. m=',m
              pause
           end if
           lengthing=lengthing+m
           do j=1,m
              sparse_numbers(node,j)=conductivity(j)
              sparse_pos(node,j)=node_connection(j)
           end do
           DEALLOCATE(conductivity)
           DEALLOCATE(node_connection)
           
         END SUBROUTINE conn
      
     

         SUBROUTINE embolize_init
      USE gcmmn
      USE nrtype
      IMPLICIT NONE
      INTEGER :: j,k,l,m,n,idum,num_pits
      DOUBLE PRECISION :: i,rnorm,max_pore,diam1,diam2
      DOUBLE PRECISION :: shared_pit_area
      DOUBLE PRECISION :: avg_rad, sum_rad,large_pore_before
      
      idum=idum_save
      
!     Set largest pore at 1nm (very small number)
      do j=1,num_segments
         large_pore_seg(j)=1.d-9
      end do
      
!     Add pores to indicate possible places to spread embolisms
      do j=1,num_segments
!     Examine pit nodes
         if(pit_tag(j).eqv..true.) then
            shared_pit_area=sampling_freq*voxel_size*2*radius(j,1)*pitlength(j) 
            num_pits=shared_pit_area*pit_fraction/(pit_height*pit_width)
            if(num_pits.eq.0) num_pits=1
            max_pore=0.d0
            do k=1,num_pits
               i=rnorm(idum)
               if(k.eq.1) then
                  max_pore=i
               end if
               if(i.gt.max_pore) max_pore=i
            end do
!     Estimated formula for embolisms
!     Mean is avg_max_pore, sqrt of variance (STDev) 
            large_pore_seg(j)=max_pore*std_pore+avg_max_pore
            if(large_pore_seg(j).lt.0) large_pore_seg(j)=-large_pore_seg(j)	
         end if
!     Examine vessel endings
         if(end_tag(j).eqv..true.) then
            large_pore_before=0
            do k=1,number_ends(j)
               sum_rad=0
               do m=1,num_edge_points(j)
                  sum_rad=sum_rad+radius(j,m)
               end do
               avg_rad=sum_rad/num_edge_points(j)
               shared_pit_area=pi*avg_rad*avg_rad
               num_pits=shared_pit_area*pit_fraction/(pit_height*pit_width)
               if(num_pits.eq.0) num_pits=1
               max_pore=0.d0
               do l=1,num_pits
                  i=rnorm(idum)
                  if(l.eq.1) then
                     max_pore=i
                  end if
                  if(i.gt.max_pore) max_pore=i
               end do
               
               !     Estimated formula for embolisms
               !     Mean is avg_max_pore, sqrt of variance (STDev) 
               large_pore_seg(j)=max_pore*std_pore+avg_max_pore
               if(large_pore_seg(j).lt.0) large_pore_seg(j)=-large_pore_seg(j) ! reflect bottom part of distribution around zero... not clean
               if((large_pore_before.lt.large_pore_seg(j)).and.(k.gt.1)) large_pore_seg(j)=large_pore_before
               large_pore_before=large_pore_seg(j)
            end do
         end if
      end do


      idum_save=idum
      END SUBROUTINE embolize_init

      SUBROUTINE calc_flow_bef
!     driver for routine linbcg
      USE gcmmn
      USE nrtype
      USE nr
      USE xlinbcg_data
      IMPLICIT NONE
      INTEGER, PARAMETER :: ITOL=1
      INTEGER :: i,j,k,m,iter,inode,counter,counter2,counter3,rev_seg,rev_node
      INTEGER :: non_pit_segs
      LOGICAL, DIMENSION (num_nodes) :: id_tag
      DOUBLE PRECISION :: err,flowtot_bef_beg
      DOUBLE PRECISION, DIMENSION(num_nodes) :: b,bcmp
      DOUBLE PRECISION :: tot_cond,length,sum,z1,z2,neg_tot
      DOUBLE PRECISION :: flowlength,percentage_neg,flowlengthmax

      z1=0
      z2=0
      do i=1,num_nodes
         if(z(i).gt.z2) z2=z(i)
         if(z(i).lt.z1) z1=z(i)
      end do
      
      id_tag=.false.
      b=0.d0
      open(unit=19,file='bottom_nodes.txt',status='old')
      open(unit=20,file='top_nodes.txt',status='old')
      do i=1,number_top_nodes
         read(20,*) j
         b(j)=top_bdry_press(j)
      end do
      do i=1,number_bott_nodes
         read(19,*) j
         b(j)=bottom_pressure
      end do
      close(19)
      close(20)

      allocate(sa%val(lengthing),sa%irow(lengthing),sa%jcol(lengthing))
      sa%n=num_nodes
      sa%len=lengthing
      counter=1
      counter2=1
      counter3=0
      
      do inode=1,num_nodes
         tot_cond=0.d0
         if(sparse_index(inode).eq.0) then !boundary node
            sa%val(counter)=1.d0
            sa%irow(counter)=inode
            sa%jcol(counter)=inode
            counter=counter+1
            counter2=counter2+1
            counter3=counter3+1
         else                   !internal node
            do i=1,sparse_index(inode)
               tot_cond=tot_cond+sparse_numbers(inode,i)
            end do
            do i=1,sparse_index(inode)
               if((sparse_pos(inode,i).gt.(inode)).and.(id_tag(inode).eqv..false.)) then
                  sa%val(counter)=tot_cond
                  sa%irow(counter)=inode
                  sa%jcol(counter)=inode
                  id_tag(inode)=.true.
                  counter=counter+1
                  counter2=counter2+1
                  sa%val(counter)=-sparse_numbers(inode,i)
                  sa%irow(counter)=inode
                  sa%jcol(counter)=sparse_pos(inode,i)
                  counter=counter+1
               elseif((sparse_pos(inode,i).lt.(inode)).and.(i.eq.sparse_index(inode).and.(id_tag(inode).eqv..false.))) then
                  sa%val(counter)=-sparse_numbers(inode,i)
                  sa%irow(counter)=inode
                  sa%jcol(counter)=sparse_pos(inode,i)
                  counter=counter+1
                  sa%val(counter)=tot_cond
                  sa%irow(counter)=inode
                  sa%jcol(counter)=inode
                  counter=counter+1
                  counter2=counter2+1
               else
                  sa%val(counter)=-sparse_numbers(inode,i)
                  sa%irow(counter)=inode
                  sa%jcol(counter)=sparse_pos(inode,i)
                  counter=counter+1
               end if
            end do
         end if
      end do
      xsol_noemb=0.d0
      

      call linbcg(b,xsol_noemb,ITOL,TOL,ITMAX,iter,err,num_nodes)
      call sprsax(sa,xsol_noemb,bcmp)
      
!     Flow rates
      do k=1,num_segments
         
         i=edge_connectivity1(k)
         j=edge_connectivity2(k)
         length=dsqrt((x(i)-x(j))**2.d0+(y(i)-y(j))**2.d0+(z(i)-z(j))**2.d0)
         
!     Take average radius
         sum=0
         
         do m=1,num_edge_points(k)
            sum=sum+radius(k,m)
         end do
         sum=sum/num_edge_points(k)

         if((z(i)-z(j)).gt.(voxel_size*0.5)) then
            if(end_tag(k).eqv..true.) then
               flowrate(k)=(xsol_noemb(j)-xsol_noemb(i))/(1/(pi*sum**4.d0/(8.d-3*length))+number_ends(k)&
                    &/(end_conductivity*pi*sum**2.d0))
            else
               flowrate(k)=pi*(sum**(4.d0))/8.d-3*(xsol_noemb(j)-xsol_noemb(i))/length
            end if
         elseif((z(j)-z(i)).gt.(voxel_size*0.5)) then
            if(end_tag(k).eqv..true.) then
               flowrate(k)=(xsol_noemb(i)-xsol_noemb(j))/(1/(pi*sum**4.d0/(8.d-3*length))+number_ends(k)&
                    &/(end_conductivity*pi*sum**2.d0))
            else
               flowrate(k)=pi*(sum**(4.d0))/8.d-3*(xsol_noemb(i)-xsol_noemb(j))/length
            end if
         else                   !pit connection or bridge connection
            if(pit_tag(k).eqv..true.) then
               flowrate(k)=ABS(pit_conductivity*shared_pit_length*2*sum*pitlength(k)*(xsol_noemb(i)-xsol_noemb(j)))
            elseif(bridge_tag(k).eqv..true.) then
               flowrate(k)=ABS(bridge_conductivity*shared_pit_length*pitlength(k)*2*sum*(xsol_noemb(i)-xsol_noemb(j)))
            end if
         end if
      end do

      if(num_initial_emb.eq.0) then
         flowlengthmax=0

         !Find longest reverse segment
         do i=1,num_segments
            if(flowrate(i).lt.0) then
               flowlength=z(edge_connectivity2(i))-z(edge_connectivity1(i))
               rev_seg=i
               rev_node=edge_connectivity1(rev_seg)
42             continue
               do j=1,num_segments
                  if(edge_connectivity2(j).eq.rev_node) then
                     if((z(edge_connectivity2(j))-z(edge_connectivity1(j))).ge.(0.5*voxel_size)) then
                        if(flowrate(j).le.0) then
                           flowlength=flowlength+z(edge_connectivity2(j))-z(edge_connectivity1(j))
                           rev_seg=j
                           rev_node=edge_connectivity1(j)
                           GOTO 42
                        end if
                     elseif(bridge_tag(j).eqv..true.) then
                        !check pressures
                        if(xsol_noemb(edge_connectivity2(J)).gt.xsol_noemb(edge_connectivity1(J))) then
                           flowlength=flowlength+z(edge_connectivity2(j))-z(edge_connectivity1(j))
                           rev_seg=j
                           rev_node=edge_connectivity1(j)
                           GOTO 42
                        end if
                     end if
                  elseif((edge_connectivity1(j).eq.rev_node).and.&
                       & (bridge_tag(j).eqv..true.).and.(j.ne.rev_seg)) then
                     if(xsol_noemb(edge_connectivity1(J)).gt.xsol_noemb(edge_connectivity2(J))) then
                        flowlength=flowlength+z(edge_connectivity2(j))-z(edge_connectivity1(j))
                        rev_seg=j
                        rev_node=edge_connectivity2(j)
                        GOTO 42
                     end if
                  end if
               end do
               if(flowlength.gt.flowlengthmax) flowlengthmax=flowlength
            end if
         end do
         
         
         j=0
         non_pit_segs=0
         !Determine percentage of vessels with a negative flow rate
         do i=1,num_segments
            if(abs((z(edge_connectivity2(i))&
                 & -z(edge_connectivity1(i)))).ge.(0.5*voxel_size)) then
               non_pit_segs=non_pit_segs+1
               if((flowrate(i).lt.0)) then
                  j=j+1
               end if
            end if
         end do

         percentage_neg=100*DBLE(j)/DBLE(non_pit_segs)
         write(31,'(F8.5,F32.8)') percentage_neg,flowlengthmax
         negstore(vess_iter)=percentage_neg
         if(vess_iter.eq.num_vess_iter) then
            neg_tot=0
            do i=1,vess_iter
               neg_tot=neg_tot+negstore(i)
            end do
            write(31,*) 'Average percentage of reverse flow segments'
            write(31,*) neg_tot/vess_iter
         end if
      end if

      flowtot_bef=0.d0
      do i=1,num_segments
         if(specify_node(edge_connectivity2(i)).eq.2) then
            flowtot_bef=flowtot_bef+flowrate(i)
          end if
      end do
      

      DEALLOCATE(sa%val,sa%irow,sa%jcol)
      END SUBROUTINE calc_flow_bef

     
      FUNCTION ran0(idum)
      INTEGER IA,IM,IQ,IR,MASK
      INTEGER, INTENT(INOUT) :: idum
      DOUBLE PRECISION :: ran0,AM
      PARAMETER (IA=16807,IM=2147483647,AM=1./IM, IQ=127773,IR=2836,MASK=123459876)
!     Minimal" random number generator of Park and Miller. Returns a uniform random deviate
!     between 0.0 and 1.0. Set or reset idum to any integer value (except the unlikely value MASK)
!     to initialize the sequence; idum must not be altered between calls for successive deviates
!     in a sequence.
      INTEGER k
      idum=ieor(idum,MASK)      !XORing with MASK allows use of zero and other simple
	k=idum/IQ		!bit patterns for idum.
      idum=IA*(idum-k*IQ)-IR*k  !Compute idum=mod(IA*idum,IM) without overflows by
      if (idum.lt.0) idum=idum+IM !Schrage's method.
      ran0=AM*idum              !Convert idum to a floating result.
	idum=ieor(idum,MASK)	!Unmask before return.
      END



      SUBROUTINE avizo_flow_before
      USE gcmmn
      IMPLICIT NONE
      INTEGER :: i,j,num_points
   
      
      
      num_points=0
      do i=1,num_segments
         num_points=num_points+num_edge_points(i)
      end do
!     num_points=num_points*num_monomers
      
      
      open(unit=85,file='avizo_flow_before.am',status='unknown')
      write(85,'(a)') '# AmiraMesh 3D ASCII 2.0'
      write(85,*)
      write(85,*)
      write(85,'("define VERTEX",i7)') num_nodes
      write(85,'(a,I7,1x)')'define EDGE', num_segments
      write(85,'(a,I7)')'define POINT', num_points
      write(85,*)
      write(85,'(a)')'Parameters {'
      write(85,'(a)')'    ContentType "HxSpatialGraph"'
      write(85,'(a)')'}'
      write(85,*)
      write(85,'(a)')'VERTEX { float[3] VertexCoordinates } @1'
      write(85,'(a)')'EDGE { int[2] EdgeConnectivity } @2'
      write(85,'(a)')'EDGE { int NumEdgePoints } @3'
      write(85,'(a)')'POINT { float[3] EdgePointCoordinates } @4'
      write(85,'(a)')'POINT { float thickness } @5'
      write(85,'(a)')'VERTEX { float Pressure } @6'
      write(85,'(a)')'EDGE { float FlowRate } @7'
      write(85,*)
      write(85,'(a)')'# Data section follows'
      write(85,'(a)')'@1'
      do 86 i=1,num_nodes
!     Convert back to microns
         write(85,'(3(ES10.4,1x))')x(i)/convert_factor, y(i)/convert_factor,z(i)/convert_factor
 86   continue
      write(85,*)
      write(85,'(a)')'@2'
      do 87 i=1,num_segments
!     Subtract 1 because numbering starts from zero
         write(85,'(2I5)') edge_connectivity1(i)-1, edge_connectivity2(i)-1
 87   continue
      write(85,*)
      write(85,'(a)')'@3'
      do 88 i=1,num_segments
         write(85,'(I4)') num_edge_points(i)
 88   continue
      write(85,*)
      write(85,'(a)')'@4'
      do 89 i=1,num_segments
         do 93 j=1,num_edge_points(i)
!     Convert back to microns
            write(85,'(3(ES10.4,1x))') coordx(i,j)/convert_factor, coordy(i,j)/convert_factor,coordz(i,j)/convert_factor
 93      continue
 89   continue
      write(85,*)
      write(85,'(a)')'@5'
      do 90 i=1,num_segments
         do 91 j=1,num_edge_points(i)
!     Convert back to microns
            write(85,'(ES10.4,1x)') radius(i,j)/convert_factor
 91      continue
 90   continue
      write(85,*)
      write(85,'(a)')'@6'
      do 92 i=1,num_nodes
         write(85,'(ES12.4,1x)') xsol_noemb(i)
 92   continue
      write(85,*)
      write(85,'(a)')'@7'
      do 94 i=1,num_segments
         write(85,'(ES10.4,1x)') flowrate(i)
 94   continue
      close(85)
      
       
      END SUBROUTINE avizo_flow_before




      SUBROUTINE avizo_export
      USE gcmmn
      IMPLICIT NONE
      INTEGER :: i,j,num_points
      CHARACTER(LEN=15) :: fileName

      num_points=0
      do i=1,num_segments
         num_points=num_points+num_edge_points(i)
      end do
!     num_points=num_points*num_monomers

      write(fileName,'(a,i5.5,a)') 'flowExp',vess_iter,'.am'

      !open(unit=85,file='avizo_flow_export.am',status='unknown')  ! act on this line if want multiple outputs...
      open(unit=85,file=fileName,status='unknown')  ! act on this line if want multiple outputs...

      write(85,'(a)') '# AmiraMesh 3D ASCII 2.0'
      write(85,*)
      write(85,*)
      write(85,'("define VERTEX",i7)') num_nodes
      write(85,'(a,I7,1x)')'define EDGE', num_segments
      write(85,'(a,I7)')'define POINT', num_points
      write(85,*)
      write(85,'(a)')'Parameters {'
      write(85,'(a)')'    ContentType "HxSpatialGraph"'
      write(85,'(a)')'}'
      write(85,*)
      write(85,'(a)')'VERTEX { float[3] VertexCoordinates } @1'
      write(85,'(a)')'EDGE { int[2] EdgeConnectivity } @2'
      write(85,'(a)')'EDGE { int NumEdgePoints } @3'
      write(85,'(a)')'POINT { float[3] EdgePointCoordinates } @4'
      write(85,'(a)')'POINT { float thickness } @5'
      write(85,'(a)')'VERTEX { float Pressure } @6'
      write(85,'(a)')'EDGE { float FlowRate } @7'
      write(85,*)
      write(85,'(a)')'# Data section follows'
      write(85,'(a)')'@1'
      do 86 i=1,num_nodes
!     Convert back to microns
         write(85,'(3(ES10.4,1x))')x(i)/convert_factor, y(i)/convert_factor,z(i)/convert_factor
 86   continue
      write(85,*)
      write(85,'(a)')'@2'
      do 87 i=1,num_segments
!     Subtract 1 because numbering starts from zero
         write(85,'(2I5)') edge_connectivity1(i)-1, edge_connectivity2(i)-1
 87   continue
      write(85,*)
      write(85,'(a)')'@3'
      do 88 i=1,num_segments
         write(85,'(I4)') num_edge_points(i)
 88   continue
      write(85,*)
      write(85,'(a)')'@4'
      do 89 i=1,num_segments
         do 93 j=1,num_edge_points(i)
!     Convert back to microns
            write(85,'(3(ES10.4,1x))') coordx(i,j)/convert_factor, coordy(i,j)/convert_factor,coordz(i,j)/convert_factor
 93      continue
 89   continue
      write(85,*)
      write(85,'(a)')'@5'
      do 90 i=1,num_segments
         do 91 j=1,num_edge_points(i)
!     Convert back to microns
            write(85,'(ES10.4,1x)') radius(i,j)/convert_factor
 91      continue
 90   continue
      write(85,*)
      write(85,'(a)')'@6'
      do 92 i=1,num_nodes
         write(85,'(ES12.5,1x)') xsol_noemb(i)
 92   continue
      write(85,*)
      write(85,'(a)')'@7'
      do 94 i=1,num_segments
         write(85,'(ES12.5,1x)') flowrate(i)
 94   continue
      close(85)
      
      END SUBROUTINE avizo_export






      SUBROUTINE linbcg(b,xsolve,itol,tol,itmax,iter,err,num_nodes)
      USE nrtype; USE nrutil, ONLY : assert_eq,nrerror
      USE nr, ONLY : atimes,asolve,snrm
      IMPLICIT NONE
      INTEGER :: num_nodes
      DOUBLE PRECISION, DIMENSION(num_nodes), INTENT(IN) :: b
      DOUBLE PRECISION, DIMENSION(num_nodes), INTENT(INOUT) :: xsolve
      INTEGER, INTENT(IN) :: itol,itmax

      DOUBLE PRECISION, INTENT(IN) :: tol
      INTEGER, INTENT(OUT) :: iter
      DOUBLE PRECISION, INTENT(OUT) :: err
      DOUBLE PRECISION, PARAMETER :: EPS=1.0e-14_dp
      INTEGER :: n
      DOUBLE PRECISION :: ak,akden,bk,bkden,bknum,bnrm,dxnrm,xnrm,zm1nrm,znrm
      DOUBLE PRECISION, DIMENSION(size(b)) :: p,pp,r,rr,z,zz
      
      n=assert_eq(size(b),size(xsolve),'linbcg')
      iter=0
      call atimes(xsolve,r,0)
      r=b-r
      rr=r
!     call atimes(r,rr,0)
      select case(itol)
      case(1)
         bnrm=snrm(b,itol)
         call asolve(r,z,0)
      case(2)
         call asolve(b,z,0)
         bnrm=snrm(z,itol)
         call asolve(r,z,0)
      case(3:4)
         call asolve(b,z,0)
         bnrm=snrm(z,itol)
         call asolve(r,z,0)
         znrm=snrm(z,itol)
      case default
         call nrerror('illegal itol in linbcg')
      end select
      do
         if (iter > itmax) exit
         iter=iter+1
         call asolve(rr,zz,1)
         bknum=dot_product(z,rr)
         if (iter == 1) then
            p=z
            pp=zz
         else
            bk=bknum/bkden
            p=bk*p+z
            pp=bk*pp+zz
         end if
         bkden=bknum
         call atimes(p,z,0)
         akden=dot_product(z,pp)
         
         ak=bknum/akden
         call atimes(pp,zz,1)
         xsolve=xsolve+ak*p
         r=r-ak*z
         rr=rr-ak*zz
         call asolve(r,z,0)
         select case(itol)
      case(1)
         err=snrm(r,itol)/bnrm
      case(2)
         err=snrm(z,itol)/bnrm
      case(3:4)
         zm1nrm=znrm
         znrm=snrm(z,itol)
         if (abs(zm1nrm-znrm) > EPS*znrm) then
            dxnrm=abs(ak)*snrm(p,itol)
            err=znrm/abs(zm1nrm-znrm)*dxnrm
         else
            err=znrm/bnrm
            cycle
         end if
         xnrm=snrm(xsolve,itol)
         
         if (err <= 0.5_dp*xnrm) then
            err=err/xnrm
         else
            err=znrm/bnrm
            cycle
         end if
      end select
!     write (*,*) ' iter=',iter,' err=',err
      if (err <= tol) exit
      end do
      END SUBROUTINE linbcg
      


      SUBROUTINE atimes(xsolve,r,itrnsp)
      USE nrtype; USE nrutil, ONLY : assert_eq
      USE nr, ONLY : sprsax,sprstx
      USE xlinbcg_data
      DOUBLE PRECISION, DIMENSION(:), INTENT(IN) :: xsolve
      DOUBLE PRECISION, DIMENSION(:), INTENT(OUT) :: r
      INTEGER, INTENT(IN) :: itrnsp
      INTEGER :: n
      n=assert_eq(size(xsolve),size(r),'atimes')
      if (itrnsp == 0) then
         call sprsax(sa,xsolve,r)
      else
         call sprstx(sa,xsolve,r)
      end if
      END SUBROUTINE atimes
      


      SUBROUTINE asolve(b,xsolve,itrnsp)
      USE nrtype; USE nrutil, ONLY : assert_eq,nrerror
      USE nr, ONLY : sprsdiag
      USE xlinbcg_data
      DOUBLE PRECISION, DIMENSION(:), INTENT(IN) :: b
      DOUBLE PRECISION, DIMENSION(:), INTENT(OUT) :: xsolve
      INTEGER, INTENT(IN) :: itrnsp
      INTEGER :: ndum
      ndum=assert_eq(size(b),size(xsolve),'asolve')
      call sprsdiag(sa,xsolve)
!	if (any(x.eq.0.0)) call nrerror('asolve: singular diagonal
!     &        matrix')
      xsolve=b/xsolve
      END SUBROUTINE asolve
      

      SUBROUTINE sprsax(sa,xsolve,b)
      USE nrtype; USE nrutil, ONLY : assert_eq,scatter_add
      IMPLICIT NONE
      TYPE(sprs2_dp), INTENT(IN) :: sa
      DOUBLE PRECISION, DIMENSION (:), INTENT(IN) :: xsolve
      DOUBLE PRECISION, DIMENSION (:), INTENT(OUT) :: b
      INTEGER :: ndum,y

      ndum=assert_eq(sa%n,size(xsolve),size(b),'sprsax_dp')
      b=0.0_dp
      call scatter_add(b,sa%val*xsolve(sa%jcol),sa%irow)
      END SUBROUTINE sprsax
      
      SUBROUTINE sprstx(sa,xsolve,b)
      USE nrtype; USE nrutil, ONLY : assert_eq,scatter_add
      IMPLICIT NONE
      TYPE(sprs2_dp), INTENT(IN) :: sa
      DOUBLE PRECISION, DIMENSION (:), INTENT(IN) :: xsolve
      DOUBLE PRECISION, DIMENSION (:), INTENT(OUT) :: b
      INTEGER :: ndum
      ndum=assert_eq(sa%n,size(xsolve),size(b),'sprstx_dp')
      b=0.0_dp
      call scatter_add(b,sa%val*xsolve(sa%jcol),sa%irow)
      END SUBROUTINE sprstx
      
      FUNCTION snrm(sx,itol)
      USE nrtype
      IMPLICIT NONE
      DOUBLE PRECISION, DIMENSION(:), INTENT(IN) :: sx
      INTEGER, INTENT(IN) :: itol
      DOUBLE PRECISION :: snrm
      if (itol <= 3) then
         snrm=sqrt(dot_product(sx,sx))
      else
         snrm=maxval(abs(sx))
      end if
      END FUNCTION snrm
      
      SUBROUTINE sprsdiag(sa,b)
      USE nrtype; USE nrutil, ONLY : array_copy,assert_eq
      IMPLICIT NONE
      TYPE(sprs2_dp), INTENT(IN) :: sa
      DOUBLE PRECISION, DIMENSION(:), INTENT(OUT) :: b
      DOUBLE PRECISION, DIMENSION(size(b)) :: val
      INTEGER :: k,l,ndum,nerr
      INTEGER, DIMENSION(size(b)) :: i
      LOGICAL, DIMENSION(:), ALLOCATABLE :: mask
      ndum=assert_eq(sa%n,size(b),'sprsdiag_dp')
      l=sa%len
      allocate(mask(l))
      mask = (sa%irow(1:l) == sa%jcol(1:l))
      call array_copy(pack(sa%val(1:l),mask),val,k,nerr)
      i(1:k)=pack(sa%irow(1:l),mask)
      deallocate(mask)
      b=0.0_dp
      b(i(1:k))=val(1:k)
      END SUBROUTINE sprsdiag
      
      

      DOUBLE PRECISION FUNCTION rnorm(idum) RESULT( fn_val )

!     Generate a random normal deviate using the polar method.
!     Reference: Marsaglia,G. & Bray,T.A. 'A convenient method for generating
!              normal variables', Siam Rev., vol.6, 260-264, 1964.

      IMPLICIT NONE


!     Local variables

      DOUBLE PRECISION:: u, sum
      DOUBLE PRECISION, SAVE      :: v, sln
      LOGICAL, SAVE   :: second = .FALSE.
      DOUBLE PRECISION, PARAMETER :: one = 1.0, vsmall = TINY( one )
      DOUBLE PRECISION :: ran0
      INTEGER :: idum
      
      IF (second) THEN
!     If second, use the second random number generated on last call

         second = .false.
         fn_val = v*sln

      ELSE
!     First call; generate a pair of random normals

         second = .true.
         DO
	    u=ran0(idum)
            v=ran0(idum)

            u = SCALE( u, 1 ) - one
            v = SCALE( v, 1 ) - one
            sum = u*u + v*v + vsmall ! vsmall added to prevent LOG(zero) / zero
            IF(sum < one) EXIT
         END DO
         sln = SQRT(- SCALE( LOG(sum), 1 ) / sum)
         fn_val = u*sln

      END IF

      RETURN
      END FUNCTION rnorm



     
      SUBROUTINE outro
      USE gcmmn  
      USE xlinbcg_data
      IMPLICIT NONE

      DEALLOCATE(coordx)
      DEALLOCATE(coordy)
      DEALLOCATE(coordz)
      DEALLOCATE(pitlength)
      DEALLOCATE(end_tag)

      close(76)
      close(81)
      close(83)


      close(84)
      close(79)
      close(31)
      DEALLOCATE(number_ends)
      DEALLOCATE(specify_node)
      DEALLOCATE(bridge_tag)
      DEALLOCATE(sparse_pos)
      DEALLOCATE(sparse_numbers)
      DEALLOCATE(sparse_index)
      DEALLOCATE(embolize_tag)
      DEALLOCATE(num_edge_points)
      DEALLOCATE(x)
      DEALLOCATE(y)
      DEALLOCATE(z)
      DEALLOCATE(radius)
      DEALLOCATE(edge_connectivity1)
      DEALLOCATE(edge_connectivity2)
      DEALLOCATE(emb_node)
      DEALLOCATE(top_bdry_press)
      DEALLOCATE(flowrate)
      DEALLOCATE(large_pore_seg)
      DEALLOCATE(bottsegs)
      DEALLOCATE(topsegs)
      DEALLOCATE(negstore)
      if(num_iterations.gt.0) then
         DEALLOCATE(PLCstore)
      end if
      DEALLOCATE(xsol_noemb)
      DEALLOCATE(noflowtag)
      DEALLOCATE(pit_tag)
      END SUBROUTINE outro

