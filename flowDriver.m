function modPix=flowDriver(eP,dirName,inds,R,VS,eInd,nEdg,nVess,szA)

    dlmwrite('top_press.txt',eP,'')
    
    !./flowPObj 

    [pts,flow]=readFlowObjFlows(dirName,sprintf('flowExp%.5d.am',1),nEdg);
    
    V=zeros(szA);
    for i=1:nVess
        if any(flow(eInd{i}))
            WMF=sum(flow(eInd{i}).*pts(eInd{i}))/sum(pts(eInd{i}));
            v=WMF/(pi*R(i).^2);
            vs=zeros(szA);
            vs(VS{i}(:,1))=VS{i}(:,2);
            V=V+vs*v;
        end
    end
    
    nA=prod(szA);
    res=[65 65];
    VIV=reshape(V,[nA 1]);
    modPix=zeros(res);
   
    for i=1:res(1)
        for j=1:res(2)
            modPix(i,j)=mean(VIV(inds{i,j}));
        end
    end
    modPix(isnan(modPix))=0;
    
    %modPix is now velocity in m/s, avearged over the pixel area
    %to obtain flow in mm^3/s:
        %multiply by 1e3
        %multiply by pixel area in mm^2
    pA=(10/128)^2;
    modPix=pA*modPix*1e3;
end