function [pts,flow]=readFlowObjFlows(dir,file,nEdge)

    fid=fopen(strcat(dir,file),'r');

    line=fgetl(fid);
    while ~strcmp(line,'@3')
        line=fgetl(fid);
    end
    pts=zeros(nEdge,1);
    i=1;
    line=fgetl(fid);
    while ~isempty(line)
        c=regexp(line,'[\d\.]+','match');
        pts(i)=str2double(c);
        i=i+1;
        line=fgetl(fid);
    end

    while ~strcmp(line,'@7')
        line=fgetl(fid);
    end

    flow=zeros(nEdge,1);
    i=1;
    line=fgetl(fid);
    while ~(isempty(line) || isnumeric(line))
        if any(regexp(line,'\*'))
            flow(i)=NaN;
        else
            c=regexp(line,'[\d\.E-+]+','match');
            flow(i)=str2double(c);
        end
        i=i+1;
        line=fgetl(fid);
    end

    fclose(fid);

end
